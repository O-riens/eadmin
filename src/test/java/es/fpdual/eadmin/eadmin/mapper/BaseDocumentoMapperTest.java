package es.fpdual.eadmin.eadmin.mapper;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import es.fpdual.eadmin.eadmin.modelo.Documento;
import es.fpdual.eadmin.eadmin.modelo.TipoDocumento;
import es.fpdual.eadmin.eadmin.modelo.Usuario;

@Transactional("eadminTransactionManager")
public abstract class BaseDocumentoMapperTest {

	private LocalDate fechaActual;
	private Usuario usuario;
	private Documento documento;

	@Autowired
	DocumentoMapper mapper;

	@Before
	public void inicializarEnCadaTest() {
		fechaActual = LocalDate.now();
		usuario = new Usuario(1, "La Pelopony", "Administrativa");
		documento = new Documento(1, "Documento de prueba", usuario, fechaActual, TipoDocumento.DOCUMENTO_PADRON);
	}

	@Test
	public void deberiaInsertarUnDocumento() throws Exception {
		// DECLARACIÓN

		// ENTRENAMIENTO

		// PRUEBA
		Integer resultado = mapper.insertarDocumento(documento);

		// COMPROBACIÓN
		assertThat(resultado, is(1));
	}

	@Test
	public void deberiaRescatarElDocumento() throws Exception {
		// DECLARACIÓN

		// ENTRENAMIENTO
		mapper.insertarDocumento(documento);

		// PRUEBA
		Documento resultado = mapper.getDocumento(1);

		// COMPROBACIÓN
		assertThat(resultado, is(documento));

	}

	@Test
	public void deberiaModificarElDocumento() throws Exception {
		// DECLARACIÓN
		Documento modificado = Documento.builder().clone(documento).withNombre("nombre actualizado").build();

		// ENTRENAMIENTO
		mapper.insertarDocumento(documento);

		// PRUEBA
		Integer resultado = mapper.actualizarDocumento(modificado);

		// COMPROBACIÓN
		assertThat(resultado, is(1));
		Documento documentoActualizado = mapper.getDocumento(1);
		assertThat(documentoActualizado, is(notNullValue()));
		assertThat(documentoActualizado.getNombre(), is("nombre actualizado"));
	}

	@Test
	public void deberiaEliminarDocumento() throws Exception {
		// DECLARACIÓN
		Documento modificado = Documento.builder().clone(documento).withNombre("nombre actualizado").build();

		// ENTRENAMIENTO
		mapper.insertarDocumento(documento);

		// PRUEBA
		Integer resultado = mapper.actualizarDocumento(modificado);

		// COMPROBACIÓN
		assertThat(resultado, is(1));
		Documento documentoActualizado = mapper.getDocumento(1);
		assertThat(documentoActualizado, is(notNullValue()));
		assertThat(documentoActualizado.getNombre(), is("nombre actualizado"));
	}

}
