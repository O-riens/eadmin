package es.fpdual.eadmin.eadmin.modelo;

import java.time.LocalDate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@EqualsAndHashCode(callSuper = false)
@Getter
@Setter
@ToString
public class Documento extends AdministracionElectronicaBase {

	private final TipoDocumento tipoDocumento;

	public Documento(Integer id, String nombre, Usuario usuario, LocalDate fechaActual, TipoDocumento tipoDocumento) {
		this.id = id;
		this.nombre = nombre;
		this.usuario = usuario;
		fechaCreacion = fechaActual;
		this.tipoDocumento = tipoDocumento;
	}

	// public TipoDocumento getTipoDocumento() {
	// return tipoDocumento;
	// }
	//
	// @Override
	// public int hashCode() {
	// return getId();
	// }
	//
	// @Override
	// public boolean equals(Object obj) {
	// if (obj instanceof Documento) {
	// final Documento documento = (Documento) obj;
	// return documento.getId() == getId();
	// }
	// return false;
	// }

	// @Override
	// public String toString() {
	// return "" + tipoDocumento;
	// }

	public static DocumentoBuilder builder() {
		return Documento.DocumentoBuilder.builder();
	}

	public static class DocumentoBuilder {

		private int id;

		private String nombre;

		private Usuario usuario;

		private LocalDate fechaCreacion;

		private TipoDocumento tipoDocumento;

		public static DocumentoBuilder builder() {
			return new DocumentoBuilder();
		}

		public Documento build() {
			return new Documento(id, nombre, usuario, fechaCreacion, tipoDocumento);
		}

		public DocumentoBuilder clone(Documento documento) {
			id = documento.getId();
			nombre = documento.getNombre();
			usuario = documento.getUsuario();
			fechaCreacion = documento.getFechaCreacion();
			tipoDocumento = documento.getTipoDocumento();
			return this;
		}

		public DocumentoBuilder withId(int id) {
			this.id = id;
			return this;
		}

		public DocumentoBuilder withNombre(String nombre) {
			this.nombre = nombre;
			return this;
		}

		public DocumentoBuilder withUsuario(Usuario usuario) {
			this.usuario = usuario;
			return this;
		}

		public DocumentoBuilder withFechaCreacion(LocalDate fechaCreacion) {
			this.fechaCreacion = fechaCreacion;
			return this;
		}

		public DocumentoBuilder withTipoDocumento(TipoDocumento tipoDocumento) {
			this.tipoDocumento = tipoDocumento;
			return this;
		}
	}
}
