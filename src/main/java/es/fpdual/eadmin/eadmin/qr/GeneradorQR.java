package es.fpdual.eadmin.eadmin.qr;

import java.io.FileOutputStream;
import java.time.LocalDate;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;

import es.fpdual.eadmin.eadmin.modelo.Documento;
import es.fpdual.eadmin.eadmin.modelo.TipoDocumento;
import es.fpdual.eadmin.eadmin.modelo.Usuario;

import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.Rectangle;

class GeneradorQR {

	public static void main(String[] args) throws Exception {
		LocalDate fechaActual = LocalDate.now();
		Usuario usuario = new Usuario(4, "Alejandro Martínez Navarro", "Contable");
		Documento documento1= new Documento(10, "Contable", usuario, fechaActual, TipoDocumento.DOCUMENTO_CONTABLE);

		
		// Paso - 1 : Creamos el documento que contendrá el código (las dimensiones son de la hoja).
		Document codigoQR = new Document(new Rectangle(360, 852));
		// Paso - 2: Ahora lo pasamos a PDF.
		PdfWriter writer = PdfWriter.getInstance(codigoQR, new FileOutputStream("codigoQR.pdf"));
		// Paso - 3: Abrimos el archivo para editarlo.
		codigoQR.open();
		// Paso - 4: Añadimos un párrafo que contendrá el nombre del documento para mostrar lo que contendrá el QR.
		codigoQR.add(new Paragraph("Nombre: " + documento1.getNombre() + "\nTipo Documento: " + documento1.getTipoDocumento()));
		// Paso - 5: Creamos el QR mediante el Barcode
		BarcodeQRCode codigo = new BarcodeQRCode("Nombre: " + documento1.getNombre() + " - " + " Tipo Documento: " + documento1.getTipoDocumento(), 1, 1, null);
		// Paso - 6: Obtenemos la imagen QR.
		Image qr_image = codigo.getImage();
		// Paso - 7: Añadimos la imagen QR dentro del documento.
		codigoQR.add(qr_image);
		// Paso - 8: Cerramos el documento.
		codigoQR.close();
	}
}